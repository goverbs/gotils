package test

import (
	"gitlab.com/goverbs/gotils/file"
	"os"
	"testing"
)

func TestFileExists(t *testing.T) {
	tempFile, err := os.CreateTemp("", "test")
	if err != nil {
		t.Fatal(err)
	}

	// Close the file descriptor
	err = tempFile.Close()
	if err != nil {
		t.Fatal(err)
	}

	defer func(name string) {
		err := os.Remove(name)
		if err != nil {
			t.Fatal(err)
		}
	}(tempFile.Name())

	if !file.FileExists(tempFile.Name()) {
		t.Errorf("FileExists(%s) = false; want true", tempFile.Name())
	}
}
