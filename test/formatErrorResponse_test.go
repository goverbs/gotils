package test

import (
	"gitlab.com/goverbs/gotils/api"
	"reflect"
	"testing"
)

func TestFormatMessageResponse_WithValidInputs(t *testing.T) {
	msg := "test message"
	code := 200
	data := map[string]string{"key": "value"}

	expected := map[string]any{
		"message": msg,
		"code":    code,
		"data":    "{\"key\":\"value\"}",
	}
	actual := api.FormatMessageResponse(msg, code, data)

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Expected %v but got %v", expected, actual)
	}
}

func TestFormatMessageResponse_WithEmptyMessage(t *testing.T) {
	msg := ""
	code := 200
	data := map[string]string{"key": "value"}

	expected := map[string]any{
		"message": msg,
		"code":    code,
		"data":    "{\"key\":\"value\"}",
	}
	actual := api.FormatMessageResponse(msg, code, data)

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Expected %v but got %v", expected, actual)
	}
}

func TestFormatMessageResponse_WithZeroCode(t *testing.T) {
	msg := "test message"
	code := 0
	data := map[string]string{"key": "value"}

	expected := map[string]any{
		"message": msg,
		"code":    code,
		"data":    "{\"key\":\"value\"}",
	}
	actual := api.FormatMessageResponse(msg, code, data)

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Expected %v but got %v", expected, actual)
	}
}

func TestFormatMessageResponse_WithEmptyData(t *testing.T) {
	msg := "test message"
	code := 200
	data := map[string]string{}

	expected := map[string]any{
		"message": msg,
		"code":    code,
		"data":    "{}",
	}
	actual := api.FormatMessageResponse(msg, code, data)

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Expected %v but got %v", expected, actual)
	}
}

func TestFormatMessageResponse_WithNilData(t *testing.T) {
	msg := "test message"
	code := 200

	expected := map[string]any{
		"message": msg,
		"code":    code,
		"data":    "null",
	}
	actual := api.FormatMessageResponse(msg, code, nil)

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Expected %v but got %v", expected, actual)
	}
}
