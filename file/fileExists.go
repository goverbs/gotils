package file

import "os"

// FileExists checks if a file exists at the given path.
// It uses the os.Stat function to get the file information and checks if an error was returned.
// If the error is of type os.IsNotExist, it means the file does not exist, so it returns false.
// Otherwise, it returns true indicating the file exists.
func FileExists(file string) bool {
	_, err := os.Stat(file)
	return !os.IsNotExist(err)
}
