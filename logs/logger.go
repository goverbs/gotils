package logs

import (
	"log"
	"os"
	"sync"
)

// Logger is an interface that defines methods for logging information, errors and warnings.
type Logger interface {
	LogInfo(message string)             // LogInfo logs an informational message.
	LogError(message string, err error) // LogError logs an error message along with the error.
	LogWarn(message string)             // LogWarn logs a warning message.
}

// logger is a struct that implements the Logger interface.
// It contains separate loggers for informational messages, errors and warnings.
type logger struct {
	infoLogger  *log.Logger // Logger for informational messages.
	errorLogger *log.Logger // Logger for error messages.
	warnLogger  *log.Logger // Logger for warning messages.
}

// once and instance are used to ensure that the logger is a singleton.
var (
	once     sync.Once
	instance Logger
)

// GetLogger returns the singleton instance of the logger.
// If the logger has not been initialized yet, it initializes it.
func GetLogger() Logger {
	once.Do(func() {
		instance = initLogger()
	})
	return instance
}

// initLogger initializes the logger.
// It creates separate loggers for informational messages, errors and warnings.
func initLogger() Logger {
	infoLogger := log.New(os.Stdout, "[INFO] ", log.Ldate|log.Ltime)
	errorLogger := log.New(os.Stdout, "[ERROR] ", log.Ldate|log.Ltime)
	warnLogger := log.New(os.Stdout, "[WARN] ", log.Ldate|log.Ltime)

	l := logger{
		infoLogger:  infoLogger,
		errorLogger: errorLogger,
		warnLogger:  warnLogger,
	}

	return &l
}

// LogInfo logs an informational message.
func (l *logger) LogInfo(message string) {
	l.infoLogger.Println(message)
}

// LogError logs an error message along with the error.
func (l *logger) LogError(message string, err error) {
	l.errorLogger.Println(message, err)
}

// LogWarn logs a warning message.
func (l *logger) LogWarn(message string) {
	l.warnLogger.Println(message)
}
