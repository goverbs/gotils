package api

import (
	"encoding/json"
	"gitlab.com/goverbs/gotils/logs"
	"reflect"
)

var l = logs.GetLogger()

// FormatMessageResponse formats a response with a message and a code.
// It returns a map with the message and code and any additional data passed to it.
// Use this function to format error responses in the API to maintain consistency.
func FormatMessageResponse(msg string, code int, data ...any) map[string]any {
	var dataStr []byte
	var err error

	// Check if data is a single map or a slice of maps
	if len(data) > 0 && data[0] != nil {
		switch reflect.TypeOf(data[0]).Kind() {
		case reflect.Map:
			dataStr, err = json.Marshal(data[0])
		case reflect.Slice:
			dataStr, err = json.Marshal(data)
		default:
			l.LogError("Invalid data type passed to FormatMessageResponse", nil)
			return nil
		}
	} else {
		dataStr = []byte("null")
	}

	if err != nil {
		l.LogError("Error formatting message response to json", err)
		return nil
	}

	message := map[string]any{
		"message": msg,
		"code":    code,
		"data":    string(dataStr),
	}

	return message
}
